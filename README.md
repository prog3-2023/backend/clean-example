# Examen Final - Programación III
### Ing. y Lic. en Sistemas (Turno Feb-Mar 2021 - 2° llamado)


### Objetivos
- Desarrollar endpoints que permitan el registro y consulta de la entidad Arbol
### Tiempo
- 2 horas reloj
### Evaluacion
- Se evaluará la versión del proyecto en el repositorio correspondiente a la hora de finalización del examen, estimada para el día 24/02/2022 11:00
- El proyecto debe compilar sin errores en cualquier entorno de programacion en el que se abra
- Todos los test unitarios deben pasar en verde
- El proyecto contempla nivel Core y Adapter (BD y API Rest)

## Consigna
#### Módulo Arboles
_Se desea implementar un backend para un microservicio que permita registrar y consultar árboles._

![arbol](https://user-images.githubusercontent.com/31078412/155318268-cf7c8407-c36f-488f-8fef-2cc4d2e0b173.png)


#### Restricciones:
- No puede existir dos árboles con el mismo nombreCientífico
- Todos los atributos de árboles son obligatorios, a excepción del nombre
- La alturaMaxima no puede ser negativa
- La edad no puede ser negativa

#### Funcionalidad
- Gestionar árbol (Crear)
  - Endpoint: POST http://localhost:8080/arboles
  - RequestBody:
    ```json
    {
      "id": null,
      "nombre": "Ceibo",
      "nombreCientifico":"Erythrina crista-galli",
      "alturaMaxima": "20.5",
      "edad": 100
    }
    ```

- Buscar árboles
  - Endpoint: GET http://localhost:8080/arboles


#### Buenas prácticas y conceptos a considerar
- La nomenclatura de paquetes será en minúsculas
- La nomenclatura de clases será en UpperCamelCase
- La nomenclatura de métodos será en lowerCamelCase
- La organización de paquetes será por modelo->aspecto, tanto a nivel src/main como a nivel src/test. Ejemplo:
  ```
  arboles
  └─ excepciones
  └─ modelo
  └─ repositorio
  └─ casodeuso
  ```
- Se deben crear pruebas unitarias en Core. No es requerido en adapter, pero se contemplará el valor agregado si se incorporan
- Usar Excepciones personalizadas
- Se debe usar método factory/instancia para crear objetos
- Nomenclatura representativa de clases, métodos, etc.
